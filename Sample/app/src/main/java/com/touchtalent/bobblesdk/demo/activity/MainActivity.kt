package com.touchtalent.bobblesdk.demo.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import com.touchtalent.bobble.core.BobbleSDK
import com.touchtalent.bobblesdk.bobble_transliteration.BobbleTransliteratorSdk
import com.touchtalent.bobblesdk.demo.databinding.ActivityMainBinding
import java.util.*

/**
 * Demo app to show-case usage of all BobbleSdk modules
 */
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //set language to download resources, if not already present
        BobbleTransliteratorSdk.setLanguages("hi_IN")

        binding.head.setOnClickListener {
            // Launch demo activity to show-case use of Head APIs
            launch(HeadDemoActivity::class.java)
        }
        binding.content.setOnClickListener {
            // Launch demo activity to show-case use of Content APIs
            launch(ContentDemoActivity::class.java)
        }
        binding.transliteration.setOnClickListener {
            // Launch demo activity to show-case use of Transliteration APIs
            launch(TransliterationDemoActivity::class.java)
        }

        BobbleSDK.setUserGender(BobbleSDK.Gender.MALE)
        BobbleSDK.setUserAge(35)
        binding.deviceId.text = getAndroidDeviceId(this)
    }

    private fun launch(activityClass: Class<*>) {
        startActivity(Intent(this, activityClass))
    }

    private fun getAndroidDeviceId(context: Context): String? {
        val INVALID_ANDROID_ID = "9774d56d682e549c"
        val androidId = Settings.Secure.getString(
            context.contentResolver,
            Settings.Secure.ANDROID_ID
        )
        return if (
            androidId == null ||
            androidId.lowercase(Locale.getDefault()) == INVALID_ANDROID_ID
        ) {
            null
        } else androidId
    }
}