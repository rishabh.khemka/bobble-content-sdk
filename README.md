# Bobble SDK

Welcome to Bobble SDK.
This repo contains guides and documentation required for integrating Bobble SDK offerings into your application.

The `Sample` folder can be checked out and compiled with Android Studio to build a sample app for the implementation

# Steps for integration

1. [core](core.md) This needs to be integrated as a one-time setup
2. [content](content.md) This needs to be integrated for including Bobble Content inside your app
3. [transliteration](transliteration.md) This needs to be integrated for including Bobble Transliteration in your app

